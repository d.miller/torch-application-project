import os
from datetime import datetime
import pandas as pd
import dotenv
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
import enum
from sqlalchemy import Column, Integer, String, Enum, Date
from bokeh.plotting import figure, output_file, save

dotenv.load_dotenv('.env.public') # Load .env environment variables
Base = declarative_base()


class OverallOutcomes(enum.Enum):
    """
    Enum object for test outcome values.
    """
    option_one = "Positive"
    option_two = "Negavite"
    option_three = "Inconclusive"

class CovidTestData(Base):
    """
    Covid test data table definition.
    """
    __tablename__ = 'covid_test_data'
    id = Column(Integer, primary_key=True, autoincrement=True)
    state = Column(String)
    state_name = Column(String)
    state_fips = Column(Integer)
    fema_region = Column(String)
    overall_outcome = Column(Enum(OverallOutcomes))
    date = Column(Date)
    new_results_reported = Column(Integer)
    total_results_reported = Column(Integer)
    geocoded_state = Column(String, nullable=True, default=None)

def seed_covid_test_data(engine):
    """
    Populate covid test csv data into table.

    Drop table if already exists
    Create table based on CovidTestData definition
    Populate data using pandas
    """
    Base.metadata.drop_all(engine, tables=[CovidTestData.__table__])
    Base.metadata.create_all(engine)
    df_data = pd.read_csv(os.environ['COVID_FILENAME'], encoding='UTF-8')
    df_data['date'] = pd.to_datetime(df_data['date'], format='%Y-%m-%d')
    df_data.to_sql(CovidTestData.__tablename__, engine, if_exists='append',index=False)
    print("COVID test data properly seeded")

def query_and_generate_results(sql_filepath, engine, output_path):
    """
    Open a filepath, load SQL into Pandas dataframe and output to html table
    """
    with open(sql_filepath) as file:
        query_statement = file.read()
    df_results = pd.read_sql(query_statement, engine)
    df_results.to_html(output_path, index=False)
    print(f"Generated query results - {sql_filepath}")

def generate_rolling_avg_line_graph(query_filepath, engine):
    """
    Generate a bokeh line graph for the 7D rolling average visualization
    """
    dates = []
    new_cases = []
    rolling_avg = []
    with open(query_filepath) as file:
        query_statement = file.read()
    results = engine.execute(query_statement)
    for result in results.fetchall():
        dates.append(datetime.strptime(result[0], '%Y-%m-%d'))
        new_cases.append(result[1])
        rolling_avg.append(result[2])
    p = figure(title="Rolling 7 Day Avg (Positive Cases-L30D)"
               , x_axis_label='Date', x_axis_type='datetime'
               ,y_axis_label='Cases', plot_width=1200)
    p.line(dates,new_cases,legend_label="New Cases", line_width=2)
    p.line(dates,rolling_avg,legend_label="Rolling Average", line_width=2, line_color="orange")
    output_file('./output/positive_results_graph.html')
    save(p)
    print("Rolling avg line graph generated")

if __name__ == '__main__':
    engine = create_engine(os.environ['DB_URI'])
    seed_covid_test_data(engine)
    query_and_generate_results(os.environ['TOTAL_TESTS_QUERY_PATH'], engine
                               , 'output/total_tests.html')
    query_and_generate_results(os.environ['POSITIVITY_ROLLING_AVERAGE_QUERY_PATH'], engine
                               , 'output/positive_results.html')
    query_and_generate_results(os.environ['TOP_TEN_POSITIVITY_QUERY_PATH'], engine
                               , 'output/top_ten.html')
    generate_rolling_avg_line_graph(os.environ['POSITIVITY_ROLLING_AVERAGE_QUERY_PATH'], engine)