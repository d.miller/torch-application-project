SELECT 
    SUM(total_results_reported) as `Total US Tests to date`
FROM covid_test_data cov 
INNER JOIN (
    SELECT 
        state
        , MAX(date) as date 
    FROM covid_test_data 
    GROUP BY state
    ) dat 
ON cov.date=dat.date AND cov.state=dat.state;