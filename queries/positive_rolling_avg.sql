WITH daily_cases as (
    SELECT 
        DATE(`date`) as `date`
        , SUM(new_results_reported) as new_cases
    FROM covid_test_data 
    WHERE overall_outcome='Positive' 
    AND date > DATE('now', '-30 day')
    GROUP BY date ORDER BY date ASC
) 
SELECT 
    date as `Date`
    , new_cases AS `New Cases`
    , CAST(AVG(new_cases) 
        OVER (ORDER BY date ASC ROWS BETWEEN 6 PRECEDING AND 0 FOLLOWING) AS Integer) as `Rolling Average`
FROM daily_cases