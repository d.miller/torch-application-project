SELECT 
    state as `State`
    , 100*ROUND(
        CAST(SUM(new_results_reported) as float) /CAST(total AS float), 4
    ) AS `Positivity Rate`
FROM covid_test_data pos 
INNER JOIN (
    SELECT state
    , sum(new_results_reported) as total 
    FROM covid_test_data WHERE date > DATE('now', '-30 day') 
GROUP BY state) tot 
USING(state) WHERE date > DATE('now', '-30 day') 
AND overall_outcome='Positive' 
GROUP BY state 
ORDER BY CAST(SUM(new_results_reported) as float) /CAST(total AS float)
DESC
LIMIT 10