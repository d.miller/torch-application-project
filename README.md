# COVID-19 Data Results

Based on the HHS Covid test data provided, contained within are some summary points derived from the results.

# Requirements and Setup
In order to replicate these results locally, the following is required
- Python3

To run the script
- Install requirements listed in requirements.txt
- While in local filepath:
     
     python main.py

This script will read the Covid test data and write it into a local Sqlite3 database. It will then run summary queries based on this dataset using the files in the 'queries' folder. Finally, it will output these results as html tables in 'output'. Additionally, a visualization of the rolling average metric will be generated into 'output'.

# Results
## General observations and caveats
While this dataset is extensive, there are some important points to consider before reviewing the data:
- This seems to be a strong representative dataset but not a completely exhaustive one (>1000 laboratories)
- The dataset is listed as provisional and subject to change
- States occasionally unload aggregated data on a specific day which can skew day-level granularity analysis
- There is a latency of 3 days in which data will be non-existent or incomplete


## Total PCR Tests (As of Yesterday)

There are a total of 851,238,399 tests conducted in the US as of yesterday according to this data. Though the dataset includes a cumulative running total of test results every day, some states are behind in this reporting. Therefore, simply summing new_daily_cases or naively taking the sum of the most recent date of data will lead to incorrect results. In which case we need to pull the cumulative total on a state-by-state basis according to each state's most recent day of reporting in order to sum up the total number of tests.

- [Link to SQL query](queries/total_tests.sql)

- [Results HTML](output/total_tests.html)
- <details>
    Total US Tests to date
    
    - 851,238,399
  </details>

## Positive Results - 7 Day Rolling Avg (L30D)
In order to get the 7 day rolling average of positive results, we need to first organize the data based on positive only outcomes summed up and organized by day. Then we can perform a secondary query using a rolling window to query a 7 day rolling average over the last 30 days.

We can see the results oscilate up and down using a 7 day window, despite trending downwards. From this, I would extrapolate that states are unloading results on a particular day which may be skewing the 7 day lookback average.  
- [Link to SQL query](queries/positive_rolling_avg.sql)

- [Results HTML](output/top_ten.html)

- [Results visualization](output/positive_results_graph.html)

- <details>
    <summary> Results </summary>
    Date 	Rolling Average

    - 2022-03-09 	23714
    - 2022-03-10 	22320
    - 2022-03-11 	21396
    - 2022-03-12 	19547
    - 2022-03-13 	17914
    - 2022-03-14 	18002
    - 2022-03-15 	18776
    - 2022-03-16 	18656
    - 2022-03-17 	18919
    - 2022-03-18 	18967
    - 2022-03-19 	18993
    - 2022-03-20 	19009
    - 2022-03-21 	19109
    - 2022-03-22 	19114
    - 2022-03-23 	19337
    - 2022-03-24 	19125
    - 2022-03-25 	19163
    - 2022-03-26 	19397
    - 2022-03-27 	19484
    - 2022-03-28 	19613
    - 2022-03-29 	19616
    - 2022-03-30 	19512
    - 2022-03-31 	19270
    - 2022-04-01 	18550
    - 2022-04-02 	17614
    - 2022-04-03 	16446
    </details>

## Top Ten Positivity Rate By State (L30D)
To get positivity rates by state, we can sum up the new_results_reported by state in which the outcome is Positive. Then we can join that to an additional table created by selecting states and their summed up new total cases. This will give us positivity rates when we can then order and limit to get the top ten in the past 30 days.

Positivity rates for the top ten states range between 3-9 percent. There is one outlier at the top of the list (IA - 100%). This appears to be a reporting issue, as IA seems to only report positive results. 

- [Link to SQL query](queries/top_ten_positivity_rates.sql)

- [Results HTML](output/top_ten.html)
- <details>
    <summary>Results</summary>
    State 	Positivity Rate
    
    - IA 	100.00
    - GU 	9.12
    - AK 	6.25
    - NM 	6.00
    - PR 	5.72
    - HI 	5.23
    - VI 	4.63
    - NV 	4.27
    - NE 	4.20
    - VT 	3.92
    </details>